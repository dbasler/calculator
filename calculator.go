package main

import (
	"fmt"
	"log"
	"strconv"
	"strings"
)

// minus is a special case for negative numbers

const beforeAddSubtractOperators = "( & ) & * & /"

func reviseFormulaSlice(formulaSlice []string, answer string, operatorIndex, leftParenIndex, rightParenIndex int) []string {
	var revisedFormulaSlice []string
	for counter := 0; counter < len(formulaSlice); counter++ {
		if counter == operatorIndex-1 {
			revisedFormulaSlice = append(revisedFormulaSlice, answer)
			continue
		}
		if counter == operatorIndex || counter == operatorIndex+1 || counter == leftParenIndex || counter == rightParenIndex {
			continue
		}
		revisedFormulaSlice = append(revisedFormulaSlice, formulaSlice[counter])
	}
	return revisedFormulaSlice
}

func indexFromOperator(multiplyIndex, divisionIndex, additionIndex, subtractionIndex int) int {
	var operatorIndex int
	if multiplyIndex > 0 {
		operatorIndex = multiplyIndex
	} else if divisionIndex > 0 {
		operatorIndex = divisionIndex
	} else if additionIndex > 0 {
		operatorIndex = additionIndex
	} else {
		operatorIndex = subtractionIndex
	}
	return operatorIndex
}

func calculateNumbers(operator string, number1 int, number2 int) string {
	var answer string
	switch operator {
	case "*":
		answer = strconv.Itoa(number1 * number2)
		break
	case "/":
		answer = strconv.Itoa(number1 / number2)
		break
	case "+":
		answer = strconv.Itoa(number1 + number2)
		break
	case "-":
		answer = strconv.Itoa(number1 - number2)
		break
	default:
		log.Fatal("Unknown operator")
		break
	}
	return answer
}

func calculate(formulaSlice []string) string {
	var hasParens bool

	var operatorIndex, leftParenIndex, rightParenIndex int
	var multiplyIndex, divisionIndex, additionIndex, subtractionIndex int

	var operator string

	operatorIndex = -1
	multiplyIndex = -1
	divisionIndex = -1
	additionIndex = -1
	subtractionIndex = -1
	leftParenIndex = -1
	rightParenIndex = -1

	if strings.ContainsAny(strings.Join(formulaSlice, ""), "( & )") {
		hasParens = true
	}
	for counter := 0; counter < len(formulaSlice); counter++ {
		if formulaSlice[counter] == "(" {
			leftParenIndex = counter
		} else if formulaSlice[counter] == ")" {
			rightParenIndex = counter
			hasParens = false
			break
		} else if formulaSlice[counter] == "*" {
			if hasParens {
				if leftParenIndex != -1 {
					multiplyIndex = counter
					break
				}
			} else {
				multiplyIndex = counter
				break
			}
		} else if formulaSlice[counter] == "/" {
			if hasParens {
				if leftParenIndex != -1 {
					divisionIndex = counter
					break
				}
			} else {
				divisionIndex = counter
				break
			}

		} else if formulaSlice[counter] == "+" {
			if hasParens {
				if leftParenIndex != -1 {
					additionIndex = counter
					break
				}
			} else {
				if !strings.ContainsAny(strings.Join(formulaSlice, ""), beforeAddSubtractOperators) {
					additionIndex = counter
					break
				}
			}
		} else if formulaSlice[counter] == "-" {
			if hasParens {
				if leftParenIndex != -1 {
					subtractionIndex = counter
					break
				}
			} else {
				if !strings.ContainsAny(strings.Join(formulaSlice, ""), beforeAddSubtractOperators) {
					subtractionIndex = counter
					break
				}
			}
		}
	}

	operatorIndex = indexFromOperator(multiplyIndex, divisionIndex, additionIndex, subtractionIndex)
	operator = formulaSlice[operatorIndex]

	number1, num1Err := strconv.Atoi(formulaSlice[operatorIndex-1])
	if num1Err != nil {
		log.Fatal(num1Err)
	}
	number2, num2Err := strconv.Atoi(formulaSlice[operatorIndex+1])
	if num2Err != nil {
		log.Fatal(num2Err)
	}

	answer := calculateNumbers(operator, number1, number2)
	revisedFormulaSlice := reviseFormulaSlice(formulaSlice, answer, operatorIndex, leftParenIndex, rightParenIndex)
	return strings.Join(revisedFormulaSlice, "")
}

func isOperator(character byte, counter int, formula string) bool {
	if string(character) == "-" && counter == 0 {
		return false
	}
	// Check if it's a negative number
	if string(character) == "-" {
		// Check if the previous element is an operator
		if string(formula[counter-1]) == "-" || string(formula[counter-1]) == "+" || string(formula[counter-1]) == "*" || string(formula[counter-1]) == "/" {
			return false
		}
	}

	if strings.ContainsAny(string(character), "( & ) & * & / & + & -") {
		return true
	}
	return false
}

func calculator(formula string) string {
	var sb strings.Builder
	var formulaSlice []string
	formula = strings.Replace(formula, " ", "", -1)

	if !strings.ContainsAny(formula, "( & ) & * & / & +") {
		if string(formula[0]) == "-" {
			var sum int
			for counter := 0; counter < len(formula); counter++ {
				if string(formula[counter]) == "-" {
					sum++
				}
				if sum > 1 {
					break
				}
			}
			if sum == 1 {
				return formula
			}
		} else if !strings.ContainsAny(formula, "-") {
			return formula
		}
	}

	for counter := 0; counter < len(formula); counter++ {
		// TODO figure out negative numbers
		if isOperator(formula[counter], counter, formula) {
			if len(sb.String()) > 0 {
				number := sb.String()
				formulaSlice = append(formulaSlice, number)
				sb.Reset()
			}
			formulaSlice = append(formulaSlice, string(formula[counter]))
		} else {
			sb.WriteString(string(formula[counter]))
			if counter == len(formula)-1 {
				number := sb.String()
				formulaSlice = append(formulaSlice, number)
				sb.Reset()
			}
		}
	}

	value := calculate(formulaSlice)
	return calculator(value)
}

func main() {
	fmt.Println(calculator("80/40+1+15+2*4+1-2*10"))
	fmt.Println(calculator("4*(3+4)"))
	fmt.Println(calculator("4*((3+4)+7)"))
	fmt.Println(calculator("10/100"))
	fmt.Println(calculator("100/10"))
	fmt.Println(calculator("-10*-10"))
	fmt.Println(calculator("-10+-10"))
	fmt.Println(calculator("-10-10"))
}
